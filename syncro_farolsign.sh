#!/bin/bash

#----------------------------------------------------------------------------#
# Script de instalação Player FarolSign                                      #
#                                                                            #
# Empresa: FarolSign                                                         #
# Linguagem: Shell Script                                                    #
# Autor: Gabriel Franco (Jamal)                                              #
#----------------------------------------------------------------------------#

#Atualizar o sistema 
	name=$(whoami);
	sudo apt-get update -y
	sudo apt-get upgrade -y
	
	name_jq=jq
	dpkg -s $name_jq &> /dev/null

		if [ $? -ne 0 ]

        then
			echo "Não instalado ... aguarde instalação"
			sleep 2
            sudo apt-get install jq -y
        else
            echo "instalado"
    fi
#Desabilitar ScreenSaver (Descanso de tela)
	sudo su
	echo -e "[SeatDefaults]\nxserver-command=X -s 0 -dpms" >> /etc/lightdm/lightdm.conf
	exit
#Baixar o instalador
#Cadastra dados
	customer=$(zenity --entry --text "Qual ID da sua Empresa?" --entry-text "");
	id=$(zenity --entry --text "Qual ID do seu ponto?" --entry-text "");

#Baixa JSON e salva em variavel
	wget -O farol.json "http://www.farolsign.com.br/rasp-script-install.php?id_customer="$customer"&id_player="$id""
	status=$(cat farol.json | jq ".status");

#exibe mensagem e baixa player
	if [ "$status" != "\"ok\"" ]
	then
		zenity --info \
		--text "ID Inválido!"
	exit
	else
		zenity --info \
		--text "ID Válido!"
		wget http://www.farolsign.com.br/downloads/tv-farolsign-node-0.1.tar 2>&1 | sed -u 's/.* \([0-9]\+%\)\ \+\([0-9.]\+.\) \(.*\)/\1\n# Baixando em \2\/s, ETA \3/' | zenity --progress --title="Baixando" --auto-close --window-icon=/usr/share/icons/gnome/48x48/status/gtk-directory.png
		mv tv-farolsign-node-0.1.tar /home/$name/Downloads/
		cd /home/$name/Downloads/
		tar -xvf tv-farolsign-node-0.1.tar
		mv tv-farolsign-node /home/$name/
		cd /home/$name/tv-farolsign-node/worker/config/
		sed -i "4c\    \"playerID\":$id," config.json
		sed -i "5c\    \"playerUID\":$id" config.json
		cd /home/$name/
	fi

#Baixar e instalar o Nodejs
	name_node=nodejs
	dpkg -s $name_node &> /dev/null

		if [ $? -ne 0 ]

        then
			echo "Não instalado ... aguarde instalação"
			sleep 2
            curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
			sudo apt-get install -y nodejs
			sudo npm install http-server -g
			sudo apt-get install -y imagemagick
		else
            echo "instalado"
    fi
#Instalar dependencias
	cd /home/$name/tv-farolsign-node/worker
	npm install
	cd /home/$name/tv-farolsign-node/player
	npm install
	cd /home/$name/
#Baixar e instalar o teamviewer no site (Host)
	name_teamviewer=teamviewer-host
	dpkg -s $name_teamviewer &> /dev/null

		if [ $? -ne 0 ]

        then
			wget https://download.teamviewer.com/download/linux/teamviewer-host_armhf.deb && mv teamviewer-host_armhf.deb /home/$name/Downloads/
			cd /home/$name/Downloads/
			sudo dpkg -i teamviewer-host_armhf.deb
			sudo apt-get upgrade -y
			sudo apt --fix-broken install -y
			sudo teamviewer license accept
			sudo teamviewer passwd 293143
			cd /home/$name/
		else
            echo "instalado"
    fi
	teamviewer info > teamviewer-status
	teamviewer version > teamviewer-version
	id_team=$(cat teamviewer-status | grep -E -o '[0-9]{10}');
	version=$(cat teamviewer-version | grep -Eo "[0-9]+\.[0-9]+");

	export SENDGRID_API_KEY='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'

	curl -d '{"personalizations": [{"to": [{"email": "suporte@farolsign.com.br"}]}],"from": {"email": "gabriel@farolsign.com.br"},"subject": "Dados teamviewer","content": [{"type": "text/plain", "value": "ID do Teamviewer:'$id_team' Versão: '$version' ID no sistema: '$id'"}]}' -H "Authorization: Bearer $SENDGRID_API_KEY" -H 'Content-Type: application/json' -X POST https://api.sendgrid.com/v3/mail/send
	rm teamviewer-status
	rm teamviewer-version
	sudo npm install pm2 -g
	cd /home/$name/tv-farolsign-node/worker/
	sudo pm2 startup
	sudo pm2 save
	sudo pm2 start sync.js
	sudo reboot