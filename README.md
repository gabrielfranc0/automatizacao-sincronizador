## Instalação
## Siga os passos para instalação:
1. cd ~
2. git clone https://gabrielfranc0@bitbucket.org/gabrielfranc0/automatizacao-sincronizador.git
3. sudo chmod +x syncro_install.sh
4. ./syncro.sh
5. colocar o ID da empresa e ID do ponto a ser instalado.
6. Aguarde a instalação :)

## Obs* Caso o sistema não abra o Player automaticamente:
1. sudo pm2 stop sync
2. ps aux | grep http-server
3. sudo kill "Número do Processo"
4. sudo pm2 restart sync